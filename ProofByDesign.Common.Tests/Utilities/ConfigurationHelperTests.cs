﻿using System;
using System.Collections.Specialized;
using System.Configuration;
using NUnit.Framework;
using ProofByDesign.Common.Utilities;

namespace ProofByDesign.Common.Tests.Utilities
{
    /// <summary>
    /// Summary description for ConfigurationHelperTests
    /// </summary>
    [TestFixture]
    public class ConfigurationHelperTests
    {
        [Test]
        public void GetAppSettingErrIfMissing()
        {
            Assert.AreEqual("Cox", ConfigurationHelper.GetAppSettingErrIfMissing("apple"));
        }

        [Test]
        [ExpectedException(typeof (ConfigurationErrorsException))]
        public void GetAppSettingErrIfMissingFails()
        {
            ConfigurationHelper.GetAppSettingErrIfMissing("dontexist");
        }

        [Test]
        public void GetAppSettingFails()
        {
            Assert.AreEqual(null, ConfigurationHelper.GetAppSetting("dontexist"));
        }

        [Test]
        public void GetBool()
        {
            Assert.AreEqual(true, ConfigurationHelper.GetAppSetting<bool>("BoolTest").Value);
        }

        [Test]
        [ExpectedException(typeof (ConfigurationErrorsException))]
        public void GetBoolFails()
        {
            ConfigurationHelper.GetAppSettingErrIfMissingOrInvalid<bool>("BadBoolTest");
        }

        [Test]
        public void GetDateTime()
        {
            Assert.AreEqual(new DateTime(2010, 02, 25).Date,
                            ConfigurationHelper.GetAppSetting<DateTime>("DateTest").Value.Date);
        }

        [Test]
        [ExpectedException(typeof (ConfigurationErrorsException))]
        public void GetDateTimeFails()
        {
            ConfigurationHelper.GetAppSettingErrIfMissingOrInvalid<DateTime>("BadDateTest");
        }

        [Test]
        public void GetDouble()
        {
            Assert.AreEqual(5.987D, ConfigurationHelper.GetAppSetting<double>("DoubleTest").Value);
        }

        [Test]
        [ExpectedException(typeof (ConfigurationErrorsException))]
        public void GetDoubleFails()
        {
            ConfigurationHelper.GetAppSettingErrIfMissingOrInvalid<double>("BadDoubleTest");
        }

        [Test]
        public void GetInt()
        {
            Assert.AreEqual(5, ConfigurationHelper.GetAppSetting<int>("IntTest").Value);
        }

        [Test]
        [ExpectedException(typeof (ConfigurationErrorsException))]
        public void GetIntFails()
        {
            ConfigurationHelper.GetAppSettingErrIfMissingOrInvalid<int>("BadIntTest");
        }

        [Test]
        public void GetSection()
        {
            NameValueCollection coll = ConfigurationHelper.GetSection("testSettings");
            Assert.IsNotNull(coll, "null colllection");
            Assert.IsTrue(coll.Count > 0, "zero colllection count");
        }

        [Test]
        [ExpectedException(typeof (ConfigurationErrorsException))]
        public void GetSectionFails()
        {
            ConfigurationHelper.GetSection("dontexist");
        }

        [Test]
        public void GetString()
        {
            Assert.AreEqual("good", ConfigurationHelper.GetAppSetting<string>("StringTest").Value);
        }
    }
}