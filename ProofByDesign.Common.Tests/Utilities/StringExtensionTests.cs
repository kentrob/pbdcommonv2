﻿using NUnit.Framework;
using ProofByDesign.Common.Utilities;

namespace ProofByDesign.Common.Tests.Utilities
{
    [TestFixture]
    public class StringExtensionTests
    {
        [Test]
        public void EllipsisWorks()
        {
            string s = "1234567890";
            Assert.AreEqual("12...", s.TruncateWithEllipsis(5));
        }

        [Test]
        public void EllipsisWorksWithEmpty()
        {
            string s = string.Empty;
            Assert.AreEqual(s.TruncateWithEllipsis(10), string.Empty);
        }

        [Test]
        public void EllipsisWorksWithNull()
        {
            string s = null;
            Assert.AreEqual(s.TruncateWithEllipsis(10), string.Empty, "null should be replaced with Empty");
        }

        [Test]
        public void EllipsisWorksWithShorterString()
        {
            string s = "12345";
            Assert.AreEqual(s, s.TruncateWithEllipsis(10));
        }

        [Test]
        public void EllipsisWorksWithStringSameLengthAfterEllipsis()
        {
            string s = "12345";
            Assert.AreEqual(s, s.TruncateWithEllipsis(2));
        }

        [Test]
        public void EllipsisWorksWithZeroLength()
        {
            string s = "12345";
            Assert.AreEqual(s, s.TruncateWithEllipsis(0));
        }

        [Test]
        public void NotNull()
        {
            string s = null;
            Assert.AreEqual(s.NotNull(), string.Empty, "null should be replaced with Empty");

            s = " ";
            Assert.AreEqual(s.NotNull(), s, "blank should not be replaced");

            s = " sdfdsf";
            Assert.AreEqual(s.NotNull(), s, "text should not be replaced");
        }

        [Test]
        public void ReplaceIfNull()
        {
            string s = null;
            Assert.AreEqual(s.ReplaceIfNull("a"), "a", "null should be replaced with 'a'");

            s = string.Empty;
            Assert.AreEqual(s.ReplaceIfNull("a"), s, "empty should not be replaced");

            s = " ";
            Assert.AreEqual(s.ReplaceIfNull("a"), s, "blank should not be replaced");

            s = " sdfdsf";
            Assert.AreEqual(s.ReplaceIfNull("a"), s, "text should not be replaced");
        }

        [Test]
        public void ReplaceIfNullOrEmptyTrimmed()
        {
            string s = null;
            Assert.AreEqual(s.ReplaceIfNullOrEmptyTrimmed("a"), "a", "null should be replaced with 'a'");

            s = string.Empty;
            Assert.AreEqual(s.ReplaceIfNullOrEmptyTrimmed("a"), "a", "empty should be replaced with 'a'");

            s = " ";
            Assert.AreEqual(s.ReplaceIfNullOrEmptyTrimmed("a"), "a", "blank should be replaced with 'a'");

            s = " sdfdsf";
            Assert.AreEqual(s.ReplaceIfNullOrEmptyTrimmed("a"), s, "text should not be replaced");
        }

        [Test]
        public void StringIsNullOrEmpty()
        {
            string s = null;

            Assert.IsTrue(s.IsNullOrEmpty(), "null should be true");

            s = string.Empty;
            Assert.IsTrue(s.IsNullOrEmpty(), "empty should be true");

            s = " ";
            Assert.IsFalse(s.IsNullOrEmpty(), "blank should be false");

            s = " sdfdsf";
            Assert.IsFalse(s.IsNullOrEmpty(), "text should be false");
        }

        [Test]
        public void StringIsNullOrEmptyTrimmed()
        {
            string s = null;

            Assert.IsTrue(s.IsNullOrEmptyTrimmed(), "null should be true");

            s = string.Empty;
            Assert.IsTrue(s.IsNullOrEmptyTrimmed(), "empty should be true");

            s = " ";
            Assert.IsTrue(s.IsNullOrEmptyTrimmed(), "blank should be true");

            s = " sdfdsf";
            Assert.IsFalse(s.IsNullOrEmptyTrimmed(), "text should be false");
        }

        [Test]
        public void TruncateWorks()
        {
            string s = "1234567890";
            Assert.AreEqual("12345", s.Truncate(5));
        }

        [Test]
        public void TruncateWorksWithEmpty()
        {
            string s = string.Empty;
            Assert.AreEqual(s.Truncate(10), string.Empty);
        }

        [Test]
        public void TruncateWorksWithNull()
        {
            string s = null;
            Assert.AreEqual(s.Truncate(10), string.Empty, "null should be replaced with Empty");
        }

        [Test]
        public void TruncateWorksWithShorterString()
        {
            string s = "12345";
            Assert.AreEqual(s, s.Truncate(10));
        }

        [Test]
        public void TruncateWorksWithZeroLength()
        {
            string s = "12345";
            Assert.AreEqual(string.Empty, s.Truncate(0));
        }


        [TestCase("TroisÉtapes", "TroisÉtapes")]
        [TestCase("A B", "A B")]
        [TestCase("A-B", "A-B")]
        [TestCase("A-BD", "A-B D")]
        [TestCase(" A-BD", " A-B D")]
        [TestCase("HiSAmBryans", "Hi S Am Bryans")]
        [TestCase("A", "A")]
        [TestCase("a", "a")]
        [TestCase(" ", " ")]
        [TestCase("_", "_")]
        [TestCase("", "")]
        [TestCase(null, null)]
        [TestCase(" z sam_bryansHello ", " z sam_bryans Hello ")]
        [TestCase("BusinessEntityUpdated 383823", "Business Entity Updated 383823")]
        [TestCase("BusinessEntityUpdated 383Ab823", "Business Entity Updated 383 Ab823")]
        public void SeparateCamelCase(string input, string expected)
        {
            Assert.AreEqual(expected, input.SeparateCamelCase());
        }

        [TestCase("  aLL Cheese is inedible", "acii", false, "en-GB")]
        [TestCase("aLL Cheese is inedible   ", "acii", false, "en-GB")]
        [TestCase("  all cheese is INnedible", "ACII", true, "en-GB")]
        [TestCase("all", "a", false, "en-GB")]
        [TestCase(null, "", false, "en-GB")]
        [TestCase("", "", false, "en-GB")]
        [TestCase("\t  \n", "", false, "en-GB")]
        [TestCase("I work from one to 7", "iwfot", false, "en-GB")]
        [TestCase("Detress articles - JPY", "daj", false, "en-GB")]
        [TestCase("L'été était une catastrophe", "léuc", false, "fr")]
        public void MakeAcronynm(string input, string expected, bool toUpper, string language = "en-GB")
        {
            Assert.AreEqual(expected, input.MakeAcronym(toUpper, language), "Input: {0}, expected {1}, toUpper {2}", input, expected, toUpper);
        }
    }
}