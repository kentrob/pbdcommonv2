﻿using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using ProofByDesign.Common.Utilities;

namespace ProofByDesign.Common.Tests.Utilities
{
    [TestFixture]
    public class DateUtilityTests
    {
        [Test]
        public void DateIsBetween()
        {
            var rangeStart = new DateTime(2000, 1, 1);
            var rangeEnd = new DateTime(2014, 12, 31);
            var date = rangeStart;
            while (date <= rangeEnd)
            {
                Assert.IsTrue(date.IsBetween(rangeStart, rangeEnd), "Wrong for {0} ({1} - {2}).", date, rangeStart,
                              rangeEnd);
                date = date.AddDays(1);
            }
        }

        [Test]
        public void DateIsBetweenWithMixedLocalAndUtc()
        {
            // British summer time = 23.59 UTC.
            var date = new DateTime(2012, 6, 2, 0, 59, 0, DateTimeKind.Local);
            var rangeStart = new DateTime(2012, 6, 1, 0, 0, 0, DateTimeKind.Utc);
            var rangeEnd = new DateTime(2012, 6, 2, 0, 0, 0, DateTimeKind.Utc);
            Assert.IsTrue(date.IsBetween(rangeStart, rangeEnd), "Wrong for {0} ({1} - {2}).", date, rangeStart, rangeEnd);
        }

        [Test]
        public void DateIsNotBetween()
        {
            var rangeStart = DateTime.Today;
            var rangeEnd = rangeStart;
            var date = rangeStart.AddDays(-1);
            Assert.IsFalse(date.IsBetween(rangeStart, rangeEnd), "Wrong for < range {0} ({1} - {2}).", date, rangeStart,
                           rangeEnd);
            date = rangeEnd.AddDays(1);
            Assert.IsFalse(date.IsBetween(rangeStart, rangeEnd), "Wrong for > range {0} ({1} - {2}).", date, rangeStart,
                           rangeEnd);
        }

        [Test]
        public void DateIsNotBetweenWithMixedLocalAndUtc()
        {
            // British summer time = 23.59 UTC.
            var date = new DateTime(2012, 6, 1, 0, 1, 0, DateTimeKind.Local);
            var rangeStart = new DateTime(2012, 6, 1, 0, 0, 0, DateTimeKind.Utc);
            var rangeEnd = new DateTime(2012, 6, 2, 0, 0, 0, DateTimeKind.Utc);
            Assert.IsFalse(date.IsBetween(rangeStart, rangeEnd), "Wrong for {0} ({1} - {2}).", date, rangeStart,
                           rangeEnd);
        }

        [Test]
        public void DayAdjective()
        {
            var days = new Dictionary<int, string>();
            days.Add(1, "First");
            days.Add(2, "Second");
            days.Add(3, "Third");
            days.Add(4, "Fourth");
            days.Add(5, "Fifth");
            days.Add(6, "Sixth");
            days.Add(7, "Seventh");
            days.Add(8, "Eighth");
            days.Add(9, "Ninth");
            days.Add(10, "Tenth");
            days.Add(11, "Eleventh");
            days.Add(12, "Twelfth");
            days.Add(13, "Thirteenth");
            days.Add(14, "Fourteenth");
            days.Add(15, "Fifteenth");
            days.Add(16, "Sixteenth");
            days.Add(17, "Seventeenth");
            days.Add(18, "Eighteenth");
            days.Add(19, "Nineteenth");
            days.Add(20, "Twentieth");
            days.Add(21, "Twenty-first");
            days.Add(22, "Twenty-second");
            days.Add(23, "Twenty-third");
            days.Add(24, "Twenty-fourth");
            days.Add(25, "Twenty-fifth");
            days.Add(26, "Twenty-sixth");
            days.Add(27, "Twenty-seventh");
            days.Add(28, "Twenty-eighth");
            days.Add(29, "Twenty-ninth");
            days.Add(30, "Thirtieth");
            days.Add(31, "Thirty-first");

            var d = new DateTime(2011, 1, 1);
            while (d.Day <= 31 && d.Month == 1)
            {
                Assert.AreEqual(days[d.Day], d.DayAdjective(true), "Wrong for {0} capitalized.", d.Day);
                Assert.AreEqual(days[d.Day].ToLowerInvariant(), d.DayAdjective(), "Wrong for {0} not-capitalized.",
                                d.Day);
                d = d.AddDays(1);
            }
        }

        [Test]
        public void DayAdjectiveShort()
        {
            var days = new Dictionary<int, string>();
            days.Add(1, "1st");
            days.Add(2, "2nd");
            days.Add(3, "3rd");
            days.Add(4, "4th");
            days.Add(5, "5th");
            days.Add(6, "6th");
            days.Add(7, "7th");
            days.Add(8, "8th");
            days.Add(9, "9th");
            days.Add(10, "10th");
            days.Add(11, "11th");
            days.Add(12, "12th");
            days.Add(13, "13th");
            days.Add(14, "14th");
            days.Add(15, "15th");
            days.Add(16, "16th");
            days.Add(17, "17th");
            days.Add(18, "18th");
            days.Add(19, "19th");
            days.Add(20, "20th");
            days.Add(21, "21st");
            days.Add(22, "22nd");
            days.Add(23, "23rd");
            days.Add(24, "24th");
            days.Add(25, "25th");
            days.Add(26, "26th");
            days.Add(27, "27th");
            days.Add(28, "28th");
            days.Add(29, "29th");
            days.Add(30, "30th");
            days.Add(31, "31st");

            var d = new DateTime(2011, 1, 1);
            while (d.Day <= 31 && d.Month == 1)
            {
                Assert.AreEqual(days[d.Day], d.DayAdjectiveShort(), "Wrong for {0} capitalized.", d.Day);
                d = d.AddDays(1);
            }
        }

        [Test]
        public void DayOfWeekCount()
        {
            Func<DateTime, DayOfWeek, int> dayCount = (date, dayOfWeek) =>
                {
                    var count = 0;
                    var monthDate = new DateTime(date.Year, date.Month, 1);
                    while (monthDate.Month == date.Month)
                    {
                        if (monthDate.DayOfWeek == dayOfWeek) count++;
                        monthDate = monthDate.AddDays(1);
                    }
                    return count;
                };

            var startDate = new DateTime(2011, 1, 1);
            var endDate = new DateTime(2054, 12, 31);
            while (startDate < endDate)
            {
                foreach (
                    var dayOfWeek in
                        Enum.GetNames(typeof(DayOfWeek)).Select(n => (DayOfWeek)Enum.Parse(typeof(DayOfWeek), n)))
                {
                    var target = DateUtility.DayOfWeekCount(startDate, dayOfWeek);
                    Assert.AreEqual(dayCount(startDate, dayOfWeek), target, "{0} is wrong for {1}, ({2})", target,
                                    startDate, dayOfWeek);
                    /*                    Console.WriteLine("There are {0} {1}s in month {2} {3}.", target, dayOfWeek, startDate.Month,
                                                          startDate.Year);*/
                }
                startDate = startDate.AddMonths(1);
            }
        }

        [Test]
        public void DayOfWeekIndex()
        {
            var startDate = new DateTime(2000, 1, 1);
            var endDate = new DateTime(2040, 12, 31);
            var testDate = startDate;

            var dayCounter = new Dictionary<DayOfWeek, int>();

            Action initDayCounters = () =>
                {
                    // initialize all daycounters to 0;
                    dayCounter.Clear();
                    foreach (
                        var e in
                            Enum.GetNames(typeof(DayOfWeek)).Select(n => (DayOfWeek)Enum.Parse(typeof(DayOfWeek), n))
                        )
                    {
                        dayCounter.Add(e, 0);
                    }
                };

            initDayCounters();
            while (testDate < endDate)
            {
                if (testDate.Day == 1) initDayCounters();
                var daysInMonth = DateTime.DaysInMonth(testDate.Year, testDate.Month);
                for (var i = 0; i < daysInMonth; i++)
                {
                    dayCounter[testDate.DayOfWeek] = dayCounter[testDate.DayOfWeek] + 1;
                    var target = testDate.DayOfWeekIndex();
                    Assert.AreEqual(dayCounter[testDate.DayOfWeek], target, "{0} is wrong for {1}, ({2})", target,
                                    testDate, testDate.DayOfWeek);
                    // Console.WriteLine("{1} is {0} {2} of the month.", target, testDate, testDate.DayOfWeek);
                    testDate = testDate.AddDays(1);
                }
            }
        }

        [Test]
        public void FirstDayOfWeekMondayTest()
        {
            // Monday June 18th 2012
            var date = new DateTime(2012, 6, 18);
            var expected = date.Date;

            for (var i = 0; i < 7; i++)
            {
                var actual = date.FirstDayOfWeek(DayOfWeek.Monday);
                Assert.AreEqual(expected, actual, "Start date was {0}", date);
                date = date.AddDays(1);
            }
        }

        private static Tuple<DateTime?, DateTime, string> DateArg(DateTime date, string expected, DateTime? now = null)
        {
            return new Tuple<DateTime?, DateTime, string>(now ?? DateTime.Now, date, expected ?? date.ToShortDateString());
        }

        public IEnumerable<Tuple<DateTime?, DateTime, string>> ElapsedDescriptionArguments => new[]
        {
             DateArg(DateTime.Now,"Just now"),
             DateArg(DateTime.Now.AddSeconds(-9),"Just now"),
             DateArg(DateTime.Now.AddMinutes(-5),"5 minutes ago"),
             DateArg(DateTime.Now.AddMinutes(-1),"1 minute ago"),
             DateArg(DateTime.Now.AddHours(-1),"1 hour ago"),
             DateArg(DateTime.Now.AddHours(-2),"2 hours ago"),
             DateArg(DateTime.Now.AddDays(-300),null),
             DateArg(DateTime.Now.AddDays(-1),"Yesterday"),
             DateArg(DateTime.Now.AddDays(-2),"2 days ago"),
             DateArg(DateTime.Now.AddDays(-700),null),
             DateArg(DateTime.Now.AddDays(-90),"90 days ago"),
             DateArg(DateTime.Now.AddDays(-91),null),
             DateArg(DateTime.Now.AddDays(-89),"89 days ago")
        };

        [TestCaseSource(nameof(ElapsedDescriptionArguments))]
        public void ElapsedDescriptionTest(Tuple<DateTime?, DateTime, string> args)
        {
            Assert.AreEqual(args.Item3, args.Item2.ElapsedDescription(args.Item1), $"Date was {args.Item3} now date was '{args.Item1}'");
        }

        [Test]
        public void ElapsedDescription_WithFutureDate_Throws()
        {
            Assert.Throws<InvalidOperationException>(() => DateTime.Now.AddSeconds(1).ElapsedDescription());
            Assert.DoesNotThrow(() => DateTime.Now.AddSeconds(0).ElapsedDescription());
        }

        [Test]
        public void FirstDayOfWeekSunday()
        {
            // Monday June 18th 2012
            var date = new DateTime(2012, 6, 18);
            var expected = date.Date.AddDays(-1);

            for (var i = 0; i < 6; i++)
            {
                var actual = date.FirstDayOfWeek(DayOfWeek.Sunday);
                Assert.AreEqual(expected, actual, "Start date was {0}", date);
                date = date.AddDays(1);
            }
        }

        [Test]
        public void LastDayOfWeekMondayTest()
        {
            // Monday June 18th 2012
            var expected = new DateTime(2012, 6, 24);
            var date = new DateTime(2012, 6, 18);

            for (var i = 0; i < 7; i++)
            {
                var actual = date.LastDayOfWeek(DayOfWeek.Sunday);
                Assert.AreEqual(expected, actual, "Start date was {0}", date);
                date = date.AddDays(1);
            }
        }

        [Test]
        public void LastDayOfWeekWednesdayTest()
        {
            var expected = new DateTime(2012, 6, 20);
            var date = expected.AddDays(-6);

            for (var i = 0; i < 7; i++)
            {
                var actual = date.LastDayOfWeek(DayOfWeek.Wednesday);
                Assert.AreEqual(expected, actual, "Start date was {0}", date);
                date = date.AddDays(1);
            }
        }

        [Test]
        public void TimeIsBetween()
        {
            var rangeStart = DateTime.Today;
            var rangeEnd = rangeStart.AddDays(1);
            var date = rangeStart;
            while (date <= rangeEnd)
            {
                Assert.IsTrue(date.IsBetweenTime(rangeStart, rangeEnd), "Wrong for {0} ({1} - {2}).", date, rangeStart,
                              rangeEnd);
                date = date.AddSeconds(1);
            }
        }

        [Test]
        public void TimeIsBetweenWithMixedLocalAndUtc()
        {
            // British summer time.
            var date = new DateTime(2012, 6, 1, 10, 30, 0, DateTimeKind.Local);
            var rangeStart = new DateTime(2012, 6, 1, 9, 30, 0, DateTimeKind.Utc);
            var rangeEnd = new DateTime(2012, 6, 1, 10, 30, 0, DateTimeKind.Utc);
            Assert.IsTrue(date.IsBetweenTime(rangeStart, rangeEnd), "Wrong for {0} ({1} - {2}).", date, rangeStart,
                          rangeEnd);
        }


        [Test]
        public void TimeIsNotBetween()
        {
            var rangeStart = DateTime.Today;
            var rangeEnd = rangeStart.AddDays(1);
            var date = rangeStart.AddSeconds(-1);
            Assert.IsFalse(date.IsBetweenTime(rangeStart, rangeEnd), "Wrong for < range {0} ({1} - {2}).", date,
                           rangeStart, rangeEnd);
            date = rangeEnd.AddSeconds(1);
            Assert.IsFalse(date.IsBetweenTime(rangeStart, rangeEnd), "Wrong for > range {0} ({1} - {2}).", date,
                           rangeStart, rangeEnd);
        }

        [Test]
        public void TimeIsNotBetweenWithMixedLocalAndUtc()
        {
            // British summer time.
            var date = new DateTime(2012, 6, 1, 10, 0, 0, DateTimeKind.Local);
            var rangeStart = new DateTime(2012, 6, 1, 9, 30, 0, DateTimeKind.Utc);
            var rangeEnd = new DateTime(2012, 6, 1, 10, 30, 0, DateTimeKind.Utc);
            Assert.IsFalse(date.IsBetweenTime(rangeStart, rangeEnd),
                           "All comparisons should be UTC. Wrong for {0} ({1} - {2}).", date, rangeStart, rangeEnd);
        }
    }
}