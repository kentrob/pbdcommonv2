﻿using System;
using NUnit.Framework;
using ProofByDesign.Common.Utilities;

namespace ProofByDesign.Common.Tests.Utilities
{
    [TestFixture]
    public class AppSettingTests
    {
        [Test]
        public void CanConvertToBoolFalse()
        {
            var ast = new AppSetting<bool>("false");
            Assert.IsTrue(ast.HasValue, "HasValue mismatch");
            Assert.AreEqual(false, ast.Value, "Value mismatch");
        }

        [Test]
        public void CanConvertToBoolTrue()
        {
            var ast = new AppSetting<bool>("true");
            Assert.IsTrue(ast.HasValue, "HasValue mismatch");
            Assert.IsFalse(ast.HasException, "HasException mismatch");
            Assert.AreEqual("true", ast.RawValue, "RawValue mismatch");
            Assert.AreEqual(true, ast.Value, "Value mismatch");
        }

        [Test]
        public void CanConvertToBoolTrueAnyCase()
        {
            var ast = new AppSetting<bool>("True");
            Assert.AreEqual(true, ast.Value, "'True' Value mismatch");
            ast = new AppSetting<bool>("TRUE");
            Assert.AreEqual(true, ast.Value, "'TRUE' Value mismatch");
        }

        [Test]
        public void CanConvertToDate()
        {
            var ast = new AppSetting<DateTime>("25 January 2010");
            Assert.IsTrue(ast.HasValue, "HasValue mismatch");
            Assert.AreEqual(new DateTime(2010, 1, 25).Date, ast.Value.Date, "Value mismatch");
        }

        [Test]
        public void CanConvertToDecimal()
        {
            var ast = new AppSetting<decimal>("5.21");
            Assert.IsTrue(ast.HasValue, "HasValue mismatch");
            Assert.IsFalse(ast.HasException, "HasException mismatch");
            Assert.AreEqual("5.21", ast.RawValue, "RawValue mismatch");
            Assert.AreEqual(5.21M, ast.Value, "Value mismatch");
        }

        [Test]
        public void CanConvertToInt()
        {
            var ast = new AppSetting<int>("5");
            Assert.IsTrue(ast.HasValue, "HasValue mismatch");
            Assert.IsFalse(ast.HasException, "HasException mismatch");
            Assert.AreEqual("5", ast.RawValue, "RawValue mismatch");
            Assert.AreEqual(5, ast.Value, "Value mismatch");
        }

        [Test]
        public void CanConvertToString()
        {
            var ast = new AppSetting<string>("any string");
            Assert.IsTrue(ast.HasValue, "HasValue mismatch");
            Assert.IsFalse(ast.HasException, "HasException mismatch");
            Assert.AreEqual("any string", ast.RawValue, "RawValue mismatch");
            Assert.AreEqual("any string", ast.Value, "Value mismatch");
        }

        [Test]
        public void CannotConvertNullToInt()
        {
            var ast = new AppSetting<int>(null);
            Assert.IsFalse(ast.HasValue, "HasValue mismatch");
            Assert.IsTrue(ast.HasException, "HasException mismatch");
            Assert.AreEqual(null, ast.RawValue, "RawValue mismatch");
        }

        [Test]
        public void NullStringIsNonValue()
        {
            var ast = new AppSetting<string>(null);
            Assert.IsFalse(ast.HasValue, "HasValue mismatch");
            Assert.IsFalse(ast.HasException, "HasException mismatch");
            Assert.AreEqual(null, ast.RawValue, "RawValue mismatch");
            Assert.AreEqual(null, ast.Value, "Value mismatch");
        }
    }
}