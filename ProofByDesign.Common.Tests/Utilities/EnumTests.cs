﻿using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using ProofByDesign.Common.Utilities;

namespace ProofByDesign.Common.Tests.Utilities
{
    [TestFixture]
    public class EnumExtensionTests
    {
        public enum TestEnum
        {
            [EnumDescription("Not selected")] NotSelected = -1,
            [EnumDescription("First one")] One = 1,
            [EnumDescription("Second one")] Two = 2,
            [EnumDescription("Third one")] Three = 3,
            Four = 4
        }

        private static void CompareDictionaries(IDictionary<string, string> expected, IDictionary<string, string> actual)
        {
            Assert.AreEqual(expected.Count, actual.Count, "List counts mismatch.");
            for (var i = 0; i < expected.Count; i++)
            {
                var expectedPair = expected.ElementAt(i);
                var actualPair = actual.ElementAt(i);
                Assert.AreEqual(expectedPair.Key, actualPair.Key, "Key mismatch. Expected {0} but found {1}.",
                    expectedPair.Key, actualPair.Key);
                Assert.AreEqual(expectedPair.Value, actualPair.Value, "Value mismatch. Expected {0} but found {1}.",
                    expectedPair.Value, actualPair.Value);
            }
        }

        private static Dictionary<string, string> GetYesNoValues()
        {
            var templList = new Dictionary<string, string>();
            foreach (var value in Enum.GetValues(typeof (YesNoType)))
            {
                var enumValue = (YesNoType) value;
                templList.Add(enumValue.ToString("d"), EnumDescriptionAttribute.Get(enumValue));
            }
            return templList;
        }

        private static IDictionary<string, string> GetTestList(string valueFormatString = "d",
            bool ignoreNegative = false)
        {
            var result = new Dictionary<string, string>();
            if (!ignoreNegative)
            {
                result.Add(TestEnum.NotSelected.ToString(valueFormatString), TestEnum.NotSelected.ToString());
            }
            result.Add(TestEnum.One.ToString(valueFormatString), TestEnum.One.ToString());
            result.Add(TestEnum.Two.ToString(valueFormatString), TestEnum.Two.ToString());
            result.Add(TestEnum.Three.ToString(valueFormatString), TestEnum.Three.ToString());
            result.Add(TestEnum.Four.ToString(valueFormatString), TestEnum.Four.ToString());
            return result;
        }

        private static IList<object> GetTestValues(bool ignoreNegative = false)
        {
            var result = new List<object>();
            if (!ignoreNegative)
            {
                result.Add(TestEnum.NotSelected);
            }
            result.Add(TestEnum.One);
            result.Add(TestEnum.Two);
            result.Add(TestEnum.Three);
            result.Add(TestEnum.Four);
            return result;
        }

        /// <summary>
        ///     Creates an enum reflection and gets its values.
        /// </summary>
        [Test]
        public void CreateEnumByReflection()
        {
            var expected = GetYesNoValues().OrderBy(kv => int.Parse(kv.Key)).ToDictionary(kvp => kvp.Key,
                kvp => kvp.Value);
            const string fullName = "ProofByDesign.Common.Tests.YesNoType,ProofByDesign.Common.Tests";
            var actual = EnumExtensions.DescriptionList(fullName, EnumDescriptionAttribute.Get, null);
            CompareDictionaries(expected, actual);
        }

        [Test]
        public void CreateEnumByReflectionWithExcludes()
        {
            var expected = GetYesNoValues().OrderBy(kv => int.Parse(kv.Key)).ToDictionary(kvp => kvp.Key,
                kvp => kvp.Value);
            expected.Remove("-1");
            const string fullName = "ProofByDesign.Common.Tests.YesNoType,ProofByDesign.Common.Tests";
            var actual = EnumExtensions.DescriptionList(fullName, EnumDescriptionAttribute.Get, new[] {-1});
            CompareDictionaries(expected, actual);
        }

        [Test]
        public void DescriptionListIsCorrect()
        {
            var expected = GetYesNoValues().OrderBy(kv => int.Parse(kv.Key)).ToDictionary(kvp => kvp.Key,
                kvp => kvp.Value);
            var actual = Enum<YesNoType>.DescriptionList(EnumDescriptionAttribute.Get);
            CompareDictionaries(expected, actual);
        }

        [Test]
        public void DescriptionListObeysExcludeList()
        {
            var expected = GetYesNoValues()
                .Where(kv => int.Parse(kv.Key) >= 0)
                .OrderBy(kv => int.Parse(kv.Key)).ToDictionary(kvp => kvp.Key, kvp => kvp.Value);
            var actual = Enum<YesNoType>.DescriptionList(EnumDescriptionAttribute.Get, new[] {-1});
            CompareDictionaries(expected, actual);
        }

        [Test]
        public void ExcludeListCanBeEmptyArray()
        {
            var expected = GetTestList();
            var actual = Enum<TestEnum>.NumericList(new int[] {});
            CompareDictionaries(expected, actual);
        }

        [Test]
        public void ExcludeListCanSpecifyNonExistentValues()
        {
            var expected = GetTestList();
            var actual = Enum<TestEnum>.NumericList(new[] {101, -345});
            CompareDictionaries(expected, actual);
        }

        [Test]
        public void GetOrderedValues()
        {
            var expected = GetTestValues();
            var actual = Enum<TestEnum>.GetOrderedValues();
            Assert.AreEqual(expected.Count, actual.Count, "List counts mismatch.");
            for (var i = 0; i < expected.Count; i++)
            {
                var expectedEnum = expected.ElementAt(i);
                var actualEnum = actual.ElementAt(i);
                Assert.AreEqual(expectedEnum, actualEnum, "Expected {0} but found {1}.", expectedEnum, actualEnum);
            }
        }

        [Test]
        public void GetOrderedValuesObeysExcludeList()
        {
            var expected = GetTestValues(true);
            var actual = Enum<TestEnum>.GetOrderedValues(new[] {-1});
            Assert.AreEqual(expected.Count, actual.Count, "List counts mismatch.");
            for (var i = 0; i < expected.Count; i++)
            {
                var expectedEnum = expected.ElementAt(i);
                var actualEnum = actual.ElementAt(i);
                Assert.AreEqual(expectedEnum, actualEnum, "Expected {0} but found {1}.", expectedEnum, actualEnum);
            }
        }

        [Test]
        public void NameListIsCorrect()
        {
            var expected = GetTestList("g");
            var actual = Enum<TestEnum>.NameList();
            CompareDictionaries(expected, actual);

            foreach (var kvp in expected)
            {
                long test;
                Assert.IsFalse(long.TryParse(kvp.Key, out test), "The key should not be the numeric value.", kvp.Key);
            }
        }

        [Test]
        public void NameListObeysExcludeList()
        {
            var expected = GetTestList("g", true);
            var actual = Enum<TestEnum>.NameList(new[] {-1});
            CompareDictionaries(expected, actual);
        }

        [Test]
        public void NumericListIsCorrect()
        {
            var expected = GetTestList();
            var actual = Enum<TestEnum>.NumericList();
            CompareDictionaries(expected, actual);
        }

        [Test]
        public void NumericListObeysExcludeList()
        {
            var expected = GetTestList(ignoreNegative: true);
            var actual = Enum<TestEnum>.NumericList(new[] {-1});
            CompareDictionaries(expected, actual);
        }

        [Test]
        [ExpectedException(typeof (ArgumentException))]
        public void ParseCanBeCaseSensitive()
        {
            Enum<TestEnum>.Parse("ONe", false);
        }

        [Test]
        public void ParseIsCaseInsensitive()
        {
            var result = Enum<TestEnum>.Parse("ONe");
            Assert.That(result, Is.EqualTo(TestEnum.One));
        }

        [Test]
        public void ParseSupportsName()
        {
            var result = Enum<TestEnum>.Parse("One");
            Assert.That(result, Is.EqualTo(TestEnum.One));
        }

        [Test]
        public void ParseSupportsValue()
        {
            var result = Enum<TestEnum>.Parse("1");
            Assert.That(result, Is.EqualTo(TestEnum.One));
        }

        [Test]
        [ExpectedException(typeof (InvalidOperationException))]
        public void ThrowUnsupportedInCodeTest()
        {
            var target = TestEnum.Three;

            switch (target)
            {
                case TestEnum.One:
                    break;
                case TestEnum.Two:
                    break;
                default:
                    Enum<TestEnum>.ThrowUnsupportedInCode(target);
                    break;
            }
        }
    }
}