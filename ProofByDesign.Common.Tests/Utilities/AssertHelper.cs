﻿using System;
using NUnit.Framework;

namespace ProofByDesign.Common.Tests.Utilities
{
    public static class AssertHelper
    {
        /// 
        /// Asserts that two dates are equal by checking the year, month, day, 
        /// hour, minute, second and millisecond components.
        /// 
        /// The current date
        /// 
        public static void AreDatesEqual(DateTime expected, DateTime actual, DateTimePrecision precision)
        {
            Assert.IsTrue((precision >= DateTimePrecision.Year && expected.Year != actual.Year),
                          "Year in dates do not match as expected.");
            Assert.IsTrue((precision >= DateTimePrecision.Month && expected.Year != actual.Month),
                          "Month in dates do not match as expected.");
            Assert.IsTrue((precision >= DateTimePrecision.Day && expected.Day != actual.Day),
                          "Day in dates do not match as expected.");
            Assert.IsTrue((precision >= DateTimePrecision.Hour && expected.Hour != actual.Hour),
                          "Hour in dates do not match as expected.");
            Assert.IsTrue((precision >= DateTimePrecision.Minute && expected.Minute != actual.Minute),
                          "Minute in dates do not match as expected.");
            Assert.IsTrue((precision >= DateTimePrecision.Second && expected.Second != actual.Second),
                          "Second in dates do not match as expected.");
            Assert.IsTrue((precision >= DateTimePrecision.Millisecond && expected.Millisecond != actual.Millisecond),
                          "Millisecond in dates do not match as expected.");
        }
    }

    public enum DateTimePrecision
    {
        Year = 0,
        Month = 1,
        Week = 2,
        Day = 3,
        Hour = 4,
        Minute = 5,
        Second = 6,
        Millisecond = 7
    }
}