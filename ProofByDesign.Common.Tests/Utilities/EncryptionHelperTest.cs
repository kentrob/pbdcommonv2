﻿using System.Text;
using NUnit.Framework;
using ProofByDesign.Common.Utilities;

namespace ProofByDesign.Common.Tests.Utilities
{
    [TestFixture]
    public class EncryptionHelperTest
    {
        [Test]
        public void MD5Hex()
        {
            const string raw = "payment_amount=1200::payment_number=12::description=Services::merchant_reference=ABC123DEFG4eAS";
            var enc = EncryptionHelper.MD5EncryptHex(raw);
            Assert.AreEqual("9a89f92fd2380dc0e27a09366c5564da", enc);
        }

        [Test]
        public void MD5HexWithAsciiEncoding()
        {
            // See http://www.villacher.net/admin/md5calc.asp and http://hash-it.net/ for encrypting samples.
            const string raw = "payment_amount=1200::payment_number=12::description=Services::merchant_reference=ABC123DEFG4eAS";
            var enc = EncryptionHelper.MD5EncryptHex(raw, new ASCIIEncoding());
            Assert.AreEqual("9a89f92fd2380dc0e27a09366c5564da", enc);
        }
    }
}