﻿using System;
using NUnit.Framework;
using ProofByDesign.Common.Utilities;

namespace ProofByDesign.Common.Tests.Utilities
{
    [TestFixture]
    public class CheckTests
    {
        //[Test, Explicit]
        //public void ComparePerformanceOfExpressionsAndArguments()
        //{
        //    const int loopCount = 10000;
        //    string blag;
        //    var s = new Stopwatch();

        //    blag = "expression";
        //    s.Start();
        //    for (var i = 0; i < loopCount; i++)
        //    {
        //        Check.NotEmpty(() => blag);                
        //    }
        //    var expElapsed = s.ElapsedMilliseconds;
        //    Console.WriteLine("{0} invocations using expression took {1} msecs", loopCount, expElapsed);

        //    blag = "arg";
        //    s.Reset();
        //    s.Start();
        //    for (var i = 0; i < loopCount; i++)
        //    {
        //        Check.NotEmpty(blag, "blag");
        //    }
        //    var argElapsed = s.ElapsedMilliseconds;
        //    Console.WriteLine("{0} invocations using argument took {1} msecs", loopCount, argElapsed);
        //}

        [TestCase("")]
        [TestCase(" ")]
        [TestCase(" \n")]
        [TestCase(null)]
        public void NotEmpty_WithEmptyString_Throws(string arg)
        {
            var o = new {Blag = arg};
            var ex = Assert.Throws<ArgumentException>(() => Check.HasValue(o.Blag, "Blag"));
            Assert.That(ex.Message.Contains("Blag"), "Name is used in exception message.");
        }

        [Test]
        public void NotNull_WithValue_DoesntThrow()
        {
            var o = new {Blag = 5};
            Assert.DoesNotThrow(() => Check.NotNull(o.Blag, "Blag"));
        }

        [Test]
        public void NotNull_WithNull_Throws()
        {
            var o = new {Blag = default(object)};
            var ex = Assert.Throws<ArgumentNullException>(() => Check.NotNull(o.Blag, "Blag"));
            Assert.That(ex.Message.Contains("Blag"), "Name is used in exception message.");
        }

        [Test]
        public void NotEmpty_WithNullString_ReportsVariableName()
        {
            var blag = "";
            var ex = Assert.Throws<ArgumentException>(() => Check.HasValue(blag, "blag"));
            Assert.That(ex.Message.Contains("blag"), "Name is used in exception message.");
        }

        [TestCase("")]
        [TestCase(" ")]
        [TestCase(" \n")]
        [TestCase(null)]
        public void NotEmpty_WithNullOrEmpty_Throws(string blag)
        {
            Assert.Throws<ArgumentException>(() => Check.HasValue(blag, "blag"));
        }

        [TestCase("a")]
        [TestCase(" z \n")]
        public void NotEmpty_WithValue_DoesntThrow(string blag)
        {
            Assert.DoesNotThrow((() => Check.HasValue(blag, "blag")));
        }
    }
}