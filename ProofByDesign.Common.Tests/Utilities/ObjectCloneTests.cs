﻿using System;
using System.Collections.Generic;
using NUnit.Framework;
using ProofByDesign.Common.Utilities;

namespace ProofByDesign.Common.Tests.Utilities
{
    /// <summary>
    /// Summary description for ObjectCloneTests
    /// </summary>
    [TestFixture]
    public class ObjectCloneTests 
    {
        private int lastId;
        private Dictionary<int, int> uniqueIds;

        private bool CheckChildCount(StructureNode sn, int childCount, int childDepth)
        {
            uniqueIds.Remove(sn.StructureId);

            if (childDepth == 0)
            {
                return true;
            }

            if (sn.Children.Count != childCount)
            {
                return false;
            }

            foreach (var child in sn.Children)
            {
                if (!CheckChildCount(child, childCount, childDepth - 1)) return false;
            }

            return true;
        }


        private StructureNode MakeStructureNode(int childCount, int childDepth)
        {
            lastId = lastId + 1;
            uniqueIds.Add(lastId, 0);

            var sn = new StructureNode(lastId);
            sn.Name = "Id_" + sn.StructureId;

            if (childDepth > 0)
            {
                for (var i = 0; i < childCount; i++)
                {
                    sn.Children.Add(MakeStructureNode(childCount, childDepth - 1));
                }
            }

            return sn;
        }

        [Serializable]
        private class StructureNode
        {
            public StructureNode(int structureId)
            {
                Children = new List<StructureNode>();
                StructureId = structureId;
            }

            public string Name { get; set; }
            public int StructureId { get; set; }
            public List<StructureNode> Children { get; set; }
        }

        [Test]
        public void StructureDeepClone()
        {
            lastId = 0;
            uniqueIds = new Dictionary<int, int>();

            var sn = MakeStructureNode(5, 2);

            Assert.IsNotNull(sn, "Node should not be null");
            Assert.AreNotEqual(0, uniqueIds.Count, "Unique ids not updated.");
            Assert.IsTrue(CheckChildCount(sn, 5, 2), "Wrong before clone");
            Assert.AreEqual(0, uniqueIds.Count, "Not all the structures were navigated.");

            var target = ObjectClone.Clone(sn);

            Assert.IsTrue(CheckChildCount(sn, 5, 2), "Wrong before clone");
        }
    }
}