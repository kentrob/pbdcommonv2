﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using NUnit.Framework;
using ProofByDesign.Common.Utilities;

namespace ProofByDesign.Common.Tests.Utilities
{
    [TestFixture]
    public class ListExtensionsTest
    {
        [Test]
        [ExpectedException(typeof (ArgumentException))]
        public void AddRangeExtNullException()
        {
            IList<int> list = new List<int>();
            IList<int> bad = null;
            list.AddRangeExt(bad);
        }

        [Test]
        [ExpectedException(typeof (NotSupportedException))]
        public void AddRangeExtWithFixedSize()
        {
            IList<int> list = new int[5];

            for (var i = 0; i < 5; i++)
            {
                list[i] = i;
            }

            var items = new[] {5, 6, 7, 8, 9};

            Assert.AreEqual(5, list.Count, "Wrong count at start.");
            list.AddRangeExt(items);
        }

        [Test]
        public void AddRangeExtWithList()
        {
            IList<int> list = new List<int>();
            for (var i = 0; i < 5; i++)
            {
                list.Add(i);
            }

            var items = new[] {5, 6, 7, 8, 9};

            Assert.AreEqual(5, list.Count, "Wrong count at start.");
            list.AddRangeExt(items);
            Assert.AreEqual(10, list.Count, "Wrong count after addrange.");

            for (var i = 0; i < 10; i++)
            {
                Assert.IsTrue(list.Contains(i), "List is missing {0}", i);
            }
        }

        [Test]
        public void AddRangeExtWithNonList()
        {
            IList<int> list = new Collection<int>();

            for (var i = 0; i < 5; i++)
            {
                list.Add(i);
            }

            var items = new[] {5, 6, 7, 8, 9};

            Assert.AreEqual(5, list.Count, "Wrong count at start.");
            list.AddRangeExt(items);
            Assert.AreEqual(10, list.Count, "Wrong count after addrange.");

            for (var i = 0; i < 10; i++)
            {
                Assert.IsTrue(list.Contains(i), "List is missing {0}", i);
            }
        }

        [Test]
        public void AsReadOnly()
        {
            IList<int> list = new List<int>();

            for (var i = 0; i < 5; i++)
            {
                list.Add(i);
            }

            Assert.IsFalse(list.IsReadOnly, "List should not be readonly before call.");
            var newList = list.AsReadOnlyExt();
            Assert.IsTrue(newList.IsReadOnly, "List not readonly after call.");
        }

        [Test]
        public void ToFormattedString_WithDefaults()
        {
            var d = new Dictionary<string, string>
            {
                {"a with b", "b"},
                {"c with null", null},
                {"", "empty key"},
            };

            const string expected = "a with b: b\nc with null: \n: empty key\n";
            var actual = d.ToList().ToFormattedString();
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void ToFormattedString_WithEmptyList()
        {
            var d = new Dictionary<string, string>();
            const string expected = "";
            var actual = d.ToList().ToFormattedString();
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void ToFormattedString_WithOverrides()
        {
            var d = new Dictionary<string, string>
            {
                {"a with b", "b"},
                {"c with null", null},
                {"", "empty key"},
            };

            const string expected = "a with b - b,\nc with null - ,\n - empty key,\n";
            var actual = d.ToList().ToFormattedString(" - ", ",\n");
            Assert.AreEqual(expected, actual);
        }
    }
}