﻿using ProofByDesign.Common.Utilities;

namespace ProofByDesign.Common.Tests
{
    public enum YesNoType
    {
        [EnumDescription("-- Please select --")] Undefined = -1,
        [EnumDescription("No")] No = 0,
        [EnumDescription("Yes")] Yes = 1
    }
}