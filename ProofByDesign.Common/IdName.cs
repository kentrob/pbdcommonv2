﻿using System;
using System.Collections.Generic;
using ProofByDesign.Common.Utilities;

namespace ProofByDesign.Common
{
    /// <summary>
    ///     This class is not persisted. It is just used for passing values around the application.
    /// </summary>
    [Serializable]
    public class IdName : IIdName, IEquatable<IdName>, IComparable
    {
        public IdName()
        {
        }

        public IdName(long id, string name)
        {
            Name = name;
            Id = id;
        }

        public IdName(IIdName otherIdName)
        {
            Check.HasValue(otherIdName, "otherIdName");
            Name = otherIdName.Name;
            Id = otherIdName.Id;
        }

        public IdName(KeyValuePair<long, string> keyValuePair)
        {
            Check.HasValue(keyValuePair, "keyValuePair");
            Name = keyValuePair.Value;
            Id = keyValuePair.Key;
        }

        public long Id { get; set; }
        public string Name { get; set; }

        public IdName AsIdName() => new IdName(this);

        public bool Equals(IdName other) => Id == other.Id;

        public override int GetHashCode() => Id.GetHashCode();

        public int CompareTo(object obj)
        {
            var other = obj as IdName;
            return other == null ? -1 : ((IComparable) Id).CompareTo(other.Id);
        }

        public static bool operator ==(IdName left, IdName right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(IdName left, IdName right)
        {
            return !Equals(left, right);
        }

        public static IdName CreateFrom(IIdName other)
        {
            Check.HasValue(other, "other");
            return new IdName(other.Id, other.Name);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((IdName) obj);
        }

        public override string ToString() => $"{Name}";

        public string ToLongString() => $"{Name} (id: {Id})";

        public bool IsValidId() => Id > 0;

        public KeyValuePair<long, string> AsKeyValuePair() => new KeyValuePair<long, string>(Id, Name);

        bool IEquatable<IdName>.Equals(IdName other) => Equals(other);
    }
}