using System;
using System.Collections.Specialized;
using System.Configuration;

namespace ProofByDesign.Common.Utilities
{
    /// <summary>
    /// Helper methods for reading the configuration.
    /// </summary>
    public static class ConfigurationHelper
    {
        #region conmmon methods

        /// <summary>
        /// Gets the named config section.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        public static NameValueCollection GetSection(string key)
        {
            var o = ConfigurationManager.GetSection(key);
            if (o == null) throw new ConfigurationErrorsException(String.Format("Section '{0}' could not be found in the configuration file.", key));
            return (NameValueCollection)o;
        }

        /// <summary>
        /// Gets the app setting by key. Returns null if key does not exist.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns>Null if key does not exist.</returns>
        public static string GetAppSetting(string key)
        {
            return GetSetting(ConfigurationManager.AppSettings, key, false);
        }

        /// <summary>
        /// Gets the app setting by key and throws an exception if key is missing from AppSettings.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        public static string GetAppSettingErrIfMissing(string key)
        {
            return GetSettingErrIfMissing(ConfigurationManager.AppSettings, key);
        }

        /// <summary>
        /// Gets the typed app setting by key. 
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns>AppSetting object.</returns>
        public static AppSetting<T> GetAppSetting<T>(string key) where T : IConvertible
        {
            return new AppSetting<T>(GetAppSetting(key));
        }

        /// <summary>
        /// Gets the typed app setting by key and throws an exception if key is missing from AppSettings
        /// or cannot be converted to the required type.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns>AppSetting object.</returns>
        public static AppSetting<T> GetAppSettingErrIfMissingOrInvalid<T>(string key) where T : IConvertible
        {
            var ast = new AppSetting<T>(GetAppSettingErrIfMissing(key));

            if (!ast.HasValue)
            {
                ThrowBadValueException(key, ast.RawValue, string.Format("Could not convert to type '{0}'.", typeof(T).Name));
            }

            return ast;
        }

        /// <summary>
        /// Gets the typed app setting by prefix and key. The app setting should be in the form
        /// 'Name="prefix.key"'.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="prefix">The prefix.</param>
        /// <param name="key">The key.</param>
        /// <returns>AppSetting object.</returns>
        public static AppSetting<T> GetPrefixedAppSetting<T>(string prefix, string key) where T : IConvertible
        {
            var pk = MakeCompositeKey(prefix, key);
            return new AppSetting<T>(GetAppSetting(pk));
        }

        /// <summary>
        /// Gets the typed app setting by prefix and key and throws an exception if key is missing from AppSettings
        /// or cannot be converted to the required type. The app setting should be in the form
        /// 'Name="prefix.key"'.        
        /// </summary>
        /// <param name="prefix">The prefix that is used in the config file, eg Fruit.Name where 'Fruit' is the prefix.</param>
        /// <param name="key">The key.</param>
        /// <returns>AppSetting object.</returns>
        public static AppSetting<T> GetPrefixedAppSettingErrIfMissingOrInvalid<T>(string prefix, string key) where T : IConvertible
        {
            var pk = MakeCompositeKey(prefix, key);
            var ast = new AppSetting<T>(GetAppSettingErrIfMissing(pk));

            if (!ast.HasValue)
            {
                ThrowBadValueException(pk, ast.RawValue, string.Format("Could not convert to type '{0}'.", typeof(T).Name));
            }

            return ast;
        }
        /// <summary>
        /// Some values are stored in the AppSettings section of the config using dot notation.
        /// This method creates the full key based on the prefix key and the setting you want. 
        /// For example: default.MySetting. 
        /// </summary>
        /// <param name="prefixKey">The first part of the key, eg 'default'.</param>
        /// <param name="settingKey">The setting part of the key, eg 'MySetting'.</param>
        /// <returns>A dot formatted string, eg 'default.MySetting'.</returns>
        private static string MakeCompositeKey(string prefixKey, string settingKey)
        {
            return String.Format("{0}.{1}", prefixKey, settingKey);
        }

        private static string GetSetting(NameValueCollection settingsCollection, string key, bool errIfMissing)
        {
            if (!errIfMissing)
            {
                return settingsCollection[key];
            }
            return GetSettingErrIfMissing(settingsCollection, key);
        }

        private static string GetSettingErrIfMissing(NameValueCollection settingsCollection, string key)
        {
            var value = settingsCollection[key];

            if (String.IsNullOrEmpty(value)) ThrowMissingValueException(key);

            return value;
        }

        private static void ThrowMissingValueException(string key)
        {
            throw new ConfigurationErrorsException(String.Format("Required value '{0}' not found in the configuration file.", key));
        }

        private static void ThrowBadValueException(string key, string value, string customMessage)
        {
            throw new ConfigurationErrorsException(String.Format("Config entry '{0}' has an invalid value, '{1}'. {2}", key, value, customMessage));
        }

        #endregion
    }
}
