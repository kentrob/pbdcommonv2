using System;
using System.Globalization;
using System.Net.Mail;
using System.Text.RegularExpressions;
using JetBrains.Annotations;

namespace ProofByDesign.Common.Utilities
{
    /// <summary>
    /// String extension methods.
    /// </summary>

    public static class StringExtensions
    {
        /// <summary>
        /// Shorthand for String.IsNullOrEmpty.
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        /// 
        [ContractAnnotation("s:null => true")]
        public static bool IsNullOrEmpty(this string s)
        {
            return string.IsNullOrEmpty(s);
        }

        /// <summary>
        /// Same as String.IsNullOrEmpty but non-null string is trimmed before testing.
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        [ContractAnnotation("s:null => true")]
        public static bool IsNullOrEmptyTrimmed(this string s)
        {
            return s == null || s.Trim().Length == 0;
        }

        /// <summary>
        /// Convenience method for string.IsNullOrWhiteSpace(s)
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        [ContractAnnotation("s:null => true")]
        public static bool IsNullOrWhitespace(this string s)
        {
            return string.IsNullOrWhiteSpace(s);
        }

        /// <summary>
        /// Returns false if guid equals Guid.Empty. Not a string method but often
        /// used in a string context.
        /// </summary>
        /// <param name="guid"></param>
        /// <returns></returns>
        public static bool HasValue(this Guid guid)
        {
            return guid != Guid.Empty;
        }

        [ContractAnnotation("s:null => false")]
        public static bool HasValue(this string s)
        {
            return !string.IsNullOrWhiteSpace(s);
        }

        /// <summary>
        /// If the input string is null, returns replacement, else returns input string.
        /// </summary>
        /// <param name="s"></param>
        /// <param name="replacement"></param>
        /// <returns></returns>
        [ContractAnnotation("s:null => notnull; s:notnull => notnull")]
        public static string ReplaceIfNull(this string s, string replacement)
        {
            return s ?? replacement;
        }


        public static string SeparateCamelCase(this string input)
        {
            return input == null ? null : Regex.Replace(input, @"(?<=\w)([A-Z])", @" $1");

        }

        /// <summary>
        /// If the input string is null, or empty after trimming, returns replacement, else returns input string.
        /// </summary>
        /// <param name="s"></param>
        /// <param name="replacement"></param>
        /// <returns></returns>
        [ContractAnnotation("s:null => notnull; s:notnull => notnull")]
        public static string ReplaceIfNullOrEmptyTrimmed(this string s, string replacement)
        {
            return s.IsNullOrEmptyTrimmed() ? replacement : s;
        }

        /// <summary>
        /// Checks whether a string is null. If so, returns string.Empty, else returns
        /// input string.
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        [ContractAnnotation("s:null => notnull; s:notnull => notnull")]
        public static string NotNull(this string s)
        {
            return ReplaceIfNull(s, string.Empty);
        }

        /// <summary>
        /// Does a regex IsMatch on the string.
        /// input string.
        /// </summary>
        /// <param name="s">The s.</param>
        /// <param name="pattern">The regex pattern.</param>
        /// <returns>
        /// 	<c>true</c> if the specified s is match; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsMatch(this string s, string pattern)
        {
            return Regex.IsMatch(s, pattern);
        }

        /// <summary>
        /// Capitalises the first letter of a string.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        [ContractAnnotation("value:null => null; value:notnull => notnull")]
        public static string InitialCapital(this string value)
        {
            if (value.IsNullOrEmpty()) return value;
            if (value.Length == 1) return value.ToUpperInvariant();
            return value.Substring(0, 1).ToUpperInvariant() + value.Substring(1).ToLowerInvariant();
        }

        /// <summary>
        /// Truncates the string to the specified length, unless the string is already
        /// shorter than that. Null strings are converted to empty.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="length">Length of the max.</param>
        /// <returns></returns>
        [ContractAnnotation("value:null => notnull; value:notnull => notnull")]
        public static string Truncate(this string value, int length)
        {
            value = value.NotNull();
            return value.Length <= length ? value : value.Substring(0, length);
        }

        /// <summary>
        /// Creates an acronym from one or more words by using the first letter of each word.
        /// </summary>
        /// <param name="value">The string to be converted.</param>
        /// <param name="asUpperCase">Returns the acronym as upper-case, the default.</param>
        /// <param name="language"></param>
        /// <returns>A string comprised of the first letter of each word</returns>
        public static string MakeAcronym(this string value, bool asUpperCase = true, string language = "en-GB")
        {
            if (value.IsNullOrWhitespace()) return "";
            Check.HasValue(language, "language");
            var ci = new CultureInfo(language, false);
            var s = value.ToLower(ci);
            s = ci.TextInfo.ToTitleCase(s);
            var r = new Regex(@"[^\p{Lu}]", RegexOptions.CultureInvariant);
            s = r.Replace(s, "");
            if (!asUpperCase)
                s = s.ToLower(ci);
            return s;
        }

        /// <summary>
        /// Truncates the value and adds an ellipsis.
        /// </summary>
        /// <param name="value">The string to truncate.</param>
        /// <param name="length">The length to truncate to. If the string is
        /// shorter or less than length, nothing is changed.</param>
        /// <returns>The truncated value with an ellipsis, or the original, if it was already
        /// within the length.</returns>
        [ContractAnnotation("value:null => notnull; value:notnull => notnull")]
        public static string TruncateWithEllipsis(this string value, int length)
        {
            value = value.NotNull();
            var newLength = length - 3;
            if (newLength < 0 || value.Length <= newLength) return value;
            return value.Truncate(newLength) + "...";
        }

        /// <summary>
        /// Capitalises the first letter of each word in a string
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        [ContractAnnotation("value:null => null; value:notnull => notnull")]
        public static string CapitaliseWords(this string value)
        {
            var array = value.ToCharArray();
            if (array.Length >= 1)
            {
                if (char.IsLower(array[0]))
                {
                    array[0] = char.ToUpper(array[0]);
                }
            }
            for (var i = 1; i < array.Length; i++)
            {
                if (array[i - 1] != ' ') continue;
                if (char.IsLower(array[i]))
                {
                    array[i] = char.ToUpper(array[i]);
                }
            }
            return new string(array);
        }

        public static bool IsValidEmailFormat(string value)
        {
            try
            {
                return value.HasValue() && (new MailAddress(value)).Address.Length > 0;
            }
            catch (FormatException)
            {
                return false;
            }
        }

        public static string ToYesNoString(this bool value, bool toLowerCase = false)
        {
            var result = value ? "Yes" : "No";
            return toLowerCase ? result.ToLowerInvariant() : result;
        }
    }
}
