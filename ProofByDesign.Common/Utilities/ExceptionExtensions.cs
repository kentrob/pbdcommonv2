﻿using System;

namespace ProofByDesign.Common.Utilities
{
    public static class ExceptionExtensions
    {
        public static Exception DeepestException(this Exception e)
        {
            var proxy = e;
            while (proxy.InnerException != null)
            {
                proxy = proxy.InnerException;
            }
            return proxy;
        }
    }
}