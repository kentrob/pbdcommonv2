using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace ProofByDesign.Common.Utilities
{
    /// <summary>
    /// This class is only here to provide the DescriptionList method that uses reflection, because
    /// we need to call it without using the generic type argument.
    /// </summary>
    public static class EnumExtensions
    {
        /// <summary>
        /// Returns a list of the enum's values and names where the value is the numeric value of the enum
        /// and the description is provided by the description delegate. The enum is created using reflection
        /// from its type fullname.
        /// </summary>
        /// <param name="enumTypeFullName">The full name of the enum type.</param>
        /// <param name="descriptionDelegate">A delegate that takes an EnumExtensions argument and returns a string.</param>
        /// <param name="excludedValues">If specified, any enum whose numeric value is specified
        /// in the array is excluded from the returned list. This is to support the creation of lists
        /// that don't include negative numbers such as 'NotDefined'.</param>
        /// <returns>
        /// A dictionary, whose key is the enum's numeric value and whose
        /// description is the string returned from the delegate.
        /// </returns>
        public static IDictionary<string, string> DescriptionList(string enumTypeFullName, Func<Enum, string> descriptionDelegate, int[] excludedValues = null)
        {
            var enumType = Type.GetType(enumTypeFullName);
            if (enumType == null)
            {
                throw new InvalidOperationException(string.Format("Could not create type '{0}'.", enumTypeFullName));
            }

            var enumExtensionType = typeof(Enum<>);
            var genericType = enumExtensionType.MakeGenericType(new[] { enumType });
            var genericInstance = Activator.CreateInstance(genericType);
            var delegateArg = descriptionDelegate;
            var result = genericType.InvokeMember("DescriptionList", BindingFlags.Default | BindingFlags.InvokeMethod,
                                                      null, genericInstance, new object[] { delegateArg, excludedValues });
            return (IDictionary<string, string>)result;
        }
    }

    /// <summary>
    /// Provides strongly-typed methods for enums. The built-in .NET ones
    /// return objects that you need to cast.
    /// The Parse and TryParse code comes from the Extension METHOD 
    /// website (http://www.extensionmethod.net/Details.aspx?ID=255).
    /// </summary>    
    /// <typeparam name="T">The type of enum to return.</typeparam>
    /// <remarks>
    /// This is not actually an extension class because you cannot extend an abstract class.
    /// </remarks>
    ///     
    public class Enum<T>
    {
        public static T Parse(string value)
        {
            return Parse(value, true);
        }

        public static T Parse(string value, bool ignoreCase)
        {
            return (T)Enum.Parse(typeof(T), value, ignoreCase);
        }

        public static bool TryParse(string value, out T returnedValue)
        {
            return TryParse(value, true, out returnedValue);
        }

        public static bool TryParse(string value, bool ignoreCase, out T returnedValue)
        {
            try
            {
                returnedValue = (T)Enum.Parse(typeof(T), value, ignoreCase);
                return true;
            }
            catch
            {
                returnedValue = default(T);
                return false;
            }
        }

        /// <summary>
        /// Throws an InvalidOperationException with an error message that the enum value is not
        /// support in code. Note that this is nothing to do with parsing but you call it when
        /// you have a switch statement for an enum and an enum value has been added since you
        /// wrote the code, so the default is to throw this exception.
        /// </summary>
        /// <param name="enumValue">The enum value.</param>
        /// <example>
        ///     MyEnum myEnumValue = MyEnum.Two;
        ///     switch (myEnumValue)
        ///     {
        ///         case MyEnum.First:
        ///             ...
        ///             break;
        ///         default:
        ///             Enum&lt;MyEnum&gt;.ThrowUnsupportedInCode(myEnumValue);
        ///             break;
        ///     }
        ///      
        /// Throws an InvalidOperationException, "Internal error. No code written to support MyEnum value 'Two'."
        /// </example>
        public static void ThrowUnsupportedInCode(T enumValue)
        {
            throw new InvalidOperationException(string.Format("Internal error. No code written to support {0} value '{1}'.", typeof(T).Name, enumValue));
        }

        /// <summary>
        /// Returns a list of the enum's values and names. The value is the numeric value not the
        /// enum name.
        /// </summary>
        /// <param name="excludedValues">If specified, any enum whose numeric value is specified
        /// in the array is excluded from the returned list. This is to support the creation of lists
        /// that don't include negative numbers such as 'NotDefined'.</param>
        /// <returns>
        /// A dictionary, whose key is a string representation of the enum's numeric
        /// value and whose description is its name.
        /// </returns>
        public static IDictionary<string, string> NumericList(int[] excludedValues = null)
        {
            return GetEnumList("d", excludedValues);
        }

        /// <summary>
        /// Returns the enum's values ordered by their numeric value as integers.
        /// The native GetValues returns them in binary order and negative values
        /// appear at the end of the list.
        /// </summary>
        /// <param name="excludedValues">If specified, any enum whose numeric value is specified
        /// in the array is excluded from the returned list. This is to support the creation of lists
        /// that don't include negative numbers such as 'NotDefined'.</param>
        /// <returns></returns>
        public static IList<object> GetOrderedValues(int[] excludedValues = null)
        {
            if (excludedValues == null) excludedValues = new int[] { };
            return Enum.GetValues(typeof(T))
                .Cast<object>()
                .Select(e => new { Val = e, Numeric = Convert.ToInt32(e) })
                .OrderBy(i => i.Numeric)
                .Where(value => !excludedValues.Contains(value.Numeric))
                .Select(value => value.Val).ToList();
        }

        /// <summary>
        /// Returns a list of the enum's values and names where the value is the name of the enum,
        /// not its numeric value. That is, both key and value are the same string.
        /// </summary>
        /// <param name="excludedValues">If specified, any enum whose numeric value is specified
        /// in the array is excluded from the returned list. This is to support the creation of lists
        /// that don't include negative numbers such as 'NotDefined'.</param>
        /// <returns>
        /// A dictionary, whose key is a string representation of the enum
        /// value and whose description is its name.
        /// </returns>
        public static IDictionary<string, string> NameList(int[] excludedValues = null)
        {
            return GetEnumList("g", excludedValues);
        }

        /// <summary>
        /// Returns a list of the enum's values and names where the value is the numeric value of the enum
        /// and the description is provided by the description delegate.
        /// </summary>
        /// <param name="descriptionDelegate">A delegate that takes an EnumExtensions argument and returns a string.</param>
        /// <param name="excludedValues">If specified, any enum whose numeric value is specified
        /// in the array is excluded from the returned list. This is to support the creation of lists
        /// that don't include negative numbers such as 'NotDefined'.</param>
        /// <returns>
        /// A dictionary, whose key is the enum's numeric value and whose 
        /// description is the string returned from the delegate.
        /// </returns>
        public static IDictionary<string, string> DescriptionList(Func<Enum, string> descriptionDelegate, int[] excludedValues = null)
        {
            var result = new Dictionary<string, string>();
            var values = GetOrderedValues(excludedValues);
            foreach (var value in values)
            {
                var enumValue = (Enum)Enum.Parse(typeof(T), value.ToString());
                var description = descriptionDelegate(enumValue);
                result.Add(enumValue.ToString("d"), description);
            }
            return result;
        }

        private static IDictionary<string, string> GetEnumList(string formatString, int[] excludedValues = null)
        {
            return GetOrderedValues(excludedValues)
                .ToDictionary(value => Enum.Format(typeof(T), value, formatString), value => Enum.GetName(typeof(T), value));
        }
    }
}
