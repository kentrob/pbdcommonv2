using System.Linq;

namespace ProofByDesign.Common.Utilities
{
    /// <summary>
    /// Extensions for IQueryable.
    /// </summary>
    public static class QueryableExtensions
    {
        /// <summary>
        /// Adds Skip and Take statements to the query.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source">The source.</param>
        /// <param name="page">The page.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <returns></returns>
        public static IQueryable<T> Paged<T>(this IQueryable<T> source, int page, int pageSize)
        {
            return source
              .Skip((page - 1) * pageSize)
              .Take(pageSize);
        }
    }
}
