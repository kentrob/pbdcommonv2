﻿using System;

namespace ProofByDesign.Common.Utilities
{
    /// <summary>
    ///     Supplies a description to an enum member.
    /// </summary>
    [AttributeUsage(AttributeTargets.Field, Inherited = false, AllowMultiple = false)]
    public class EnumDescriptionAttribute : Attribute
    {
        private readonly string description;
        private readonly string oneWordDescription;

        public EnumDescriptionAttribute(string description, string oneWordDescription = null)
        {
            this.description = description;
            this.oneWordDescription = oneWordDescription;
        }

        public string Description
        {
            get { return description; }
        }

        public string OneWordDescription
        {
            get { return oneWordDescription; }
        }

        /// <summary>
        ///     Get the value of the EnumDescription attribute. If that is not found returns
        ///     the string value of the enum.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string Get(Enum value)
        {
            if (value == null) return null;
            var attr = GetAttribute(value);
            return attr == null ? value.ToString() : attr.Description ?? value.ToString();
        }

        /// <summary>
        ///     Get the value of the EnumOneWordDescription attribute. If that is not found returns
        ///     the string value of the enum.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string GetOneWord(Enum value)
        {
            if (value == null) return null;
            var attr = GetAttribute(value);
            return attr == null ? value.ToString() : attr.OneWordDescription ?? value.ToString();
        }

        public static EnumDescriptionAttribute GetAttribute(Enum value)
        {
            if (value == null) return null;
            var type = value.GetType();
            var info = type.GetMember(value.ToString());
            if (info.Length == 1)
            {
                return GetCustomAttribute(info[0], typeof (EnumDescriptionAttribute)) as EnumDescriptionAttribute;
            }
            return null;
        }
    }
}