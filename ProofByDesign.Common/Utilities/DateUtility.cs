using System;
using System.Collections.Generic;
using System.Globalization;

namespace ProofByDesign.Common.Utilities
{
    /// <summary>
    ///     Utility class that allows us to avoid the problems associated with database-specific
    ///     date behaviour, e.g. .NET MinDate is out of range for Sql Server dates.
    /// </summary>
    public static class DateUtility
    {
        static DateUtility()
        {
            DefaultDate = MinDate;
        }

        #region Public methods

        /// <summary>
        ///     A default date to use in the application
        /// </summary>
        /// <remarks>Configured by Dates.Default, and defaults to MinDate if not set</remarks>
        public static DateTime DefaultDate { get; }

        /// <summary>
        ///     The minimum date value to use
        /// </summary>
        /// <remarks>Configured by Dates.MinDate, and defaults to 1 Jan 1753 (SQL Server min allowed value)</remarks>
        public static DateTime MinDate { get; } = new DateTime(1753, 1, 1, 0, 0, 0, DateTimeKind.Utc);


        /// <summary>
        ///     The maximum date value to use
        /// </summary>
        /// <remarks>Configured by Dates.MaxDate and defaults to 31 Dec 9999</remarks>
        public static DateTime MaxDate { get; } = new DateTime(9999, 12, 31, 0, 0, 0, DateTimeKind.Utc);

        /// <summary>
        ///     Returns a date between <see cref="MinDate" /> and <see cref="MaxDate" /> rounded to database precision.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static DateTime Round(DateTime value)
        {
            if (value < MinDate)
            {
                return MinDate;
            }

            if (value > MaxDate)
            {
                return MaxDate;
            }

            // Strip the milliseconds - 10000 ticks = 1ms
            const long stripMs = 10000000;
            var ticks = value.Ticks/stripMs;
            var newValue = new DateTime(ticks*stripMs, value.Kind);
            return newValue;
        }

        #endregion

        #region extension methods

        public static DateTime MinDateIfNull(this DateTime? dateTime)
        {
            return NotNull(dateTime);
        }

        public static DateTime NotNull(DateTime? dateTime)
        {
            return dateTime ?? MinDate;
        }

        public static DateTime DatabaseRound(this DateTime dateTime)
        {
            return Round(dateTime);
        }

        public static DateTime DateMinValueRounded => MinDate.DatabaseRound();

        /// <summary>
        ///     Returns the date of the first day of the week for the specified date. The
        ///     first day of the week is culturally specific and startOfWeekDay determines
        ///     the one you want.
        /// </summary>
        /// <param name="date">The date.</param>
        /// <param name="startOfWeekDay">The day considered to be the start of the week.</param>
        /// <returns></returns>
        public static DateTime FirstDayOfWeek(this DateTime date, DayOfWeek startOfWeekDay)
        {
            var startOfWeekDate = date.Date;
            while (startOfWeekDate.DayOfWeek != startOfWeekDay)
            {
                startOfWeekDate = startOfWeekDate.AddDays(-1);
            }
            return startOfWeekDate;
        }

        /// <summary>
        ///     Returns the date of the first day of the week for the specified date. Monday is
        ///     considered to be the first day.
        /// </summary>
        /// <param name="date">The date.</param>
        /// <returns></returns>
        public static DateTime FirstDayOfWeek(this DateTime date)
        {
            return FirstDayOfWeek(date, DayOfWeek.Monday);
        }

        /// <summary>
        ///     Returns the date of the last day of the week for the specified date. The
        ///     last day of the week is culturally specific and endOfWeekDay determines
        ///     the one you want.
        /// </summary>
        /// <param name="date">The date.</param>
        /// <param name="endOfWeekDay">The day considered to be the end of the week.</param>
        /// <returns></returns>
        public static DateTime LastDayOfWeek(this DateTime date, DayOfWeek endOfWeekDay)
        {
            var endOfWeekDate = date.Date;
            while (endOfWeekDate.DayOfWeek != endOfWeekDay)
            {
                endOfWeekDate = endOfWeekDate.AddDays(1);
            }
            return endOfWeekDate;
        }

        /// <summary>
        ///     Returns the date of the last day of the week for the specified date. Sunday is
        ///     considered to be the last day.
        /// </summary>
        /// <param name="date">The date.</param>
        /// <returns></returns>
        public static DateTime LastDayOfWeek(this DateTime date)
        {
            return LastDayOfWeek(date, DayOfWeek.Sunday);
        }

        /// <summary>
        ///     Returns the same date with its time set to 23.59.59.
        /// </summary>
        /// <param name="date">The date.</param>
        /// <returns></returns>
        public static DateTime LastSecondOfDay(this DateTime date)
        {
            return new DateTime(date.Year, date.Month, date.Day, 23, 59, 59);
        }

        /// <summary>
        ///     Returns the same date with its time set to 00.00.00.
        /// </summary>
        /// <param name="date">The date.</param>
        /// <returns></returns>
        public static DateTime FirstSecondOfDay(this DateTime date)
        {
            return date.Date;
        }

        /// <summary>
        ///     Determines whether the specified date is between two other dates, excluding the time portion.
        ///     The date is converted to UTC for comparison so ensure that your date has the correct
        ///     Kind setting.
        /// </summary>
        /// <param name="date">The date.</param>
        /// <param name="rangeStart">The range start.</param>
        /// <param name="rangeEnd">The range end.</param>
        /// <returns>
        ///     <c>true</c> if the specified date is between; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsBetween(this DateTime date, DateTime rangeStart, DateTime rangeEnd)
        {
            return date.Date.ToUniversalTime() >= rangeStart.Date.ToUniversalTime()
                   && date.Date.ToUniversalTime() <= rangeEnd.Date.ToUniversalTime();
        }

        /// <summary>
        ///     Determines whether the specified date is between two other dates, including the time portion.
        ///     The date is converted to UTC for comparison so ensure that your date has the correct
        ///     Kind setting.
        /// </summary>
        /// <param name="date">The date.</param>
        /// <param name="rangeStart">The range start.</param>
        /// <param name="rangeEnd">The range end.</param>
        /// <returns>
        ///     <c>true</c> if the specified date is between; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsBetweenTime(this DateTime date, DateTime rangeStart, DateTime rangeEnd)
        {
            return date.ToUniversalTime() >= rangeStart.ToUniversalTime()
                   && date.ToUniversalTime() <= rangeEnd.ToUniversalTime();
        }


        public static string ElapsedDescription(this DateTime date, DateTime? nowDate = null)
        {
            var now = nowDate ?? DateTime.Now;

            if (date > now)
                throw new InvalidOperationException(
                    $"Date cannot be in the future. Date was:{date}, Current date:{now}.");


            var span = now - date;

            if (Math.Abs(span.TotalSeconds) < 10) return "Just now";

            Func<int, string> s = x => x == 1 ? "" : "s";

            var days = span.Days;

            if (days == 0)
            {
                return span.Hours == 0
                    ? (span.Minutes == 0
                        ? span.Seconds + " second" + s(span.Seconds) + " ago"
                        : span.Minutes + " minute" + s(span.Minutes) + " ago")
                    : span.Hours + " hour" + s(span.Hours) + " ago";
            }

            if (days > 90) return date.ToShortDateString();

            return days == 1 ? "Yesterday" : $"{span.Days} days ago";
        }


        /// <summary>
        ///     Calculates the 'day of week' index in the month the
        ///     current date represents. Takes the day of week of the date and works
        ///     out where it is in the series, eg '3rd Friday'.
        /// </summary>
        /// <param name="date">The date.</param>
        /// <returns></returns>
        public static int DayOfWeekIndex(this DateTime date)
        {
            var dayOfWeek = date.DayOfWeek;
            var dayOfMonth = new DateTime(date.Year, date.Month, 1);
            var count = 0;

            while (dayOfMonth <= date.Date)
            {
                if (dayOfMonth.DayOfWeek == dayOfWeek)
                {
                    count++;
                }
                dayOfMonth = dayOfMonth.AddDays(1);
            }

            return count;
        }

        /// <summary>
        ///     Describes the day as an adjective, eg 'first, tenth, twenty-ninth'.
        /// </summary>
        /// <param name="date">The date.</param>
        /// <param name="capitalize">if set to <c>true</c> capitalizes the string.</param>
        /// <returns></returns>
        public static string DayAdjective(this DateTime date, bool capitalize = false)
        {
            var adjectives = new Dictionary<int, string>();
            adjectives.Add(1, "first");
            adjectives.Add(2, "second");
            adjectives.Add(3, "third");
            adjectives.Add(4, "fourth");
            adjectives.Add(5, "fifth");
            adjectives.Add(6, "sixth");
            adjectives.Add(7, "seventh");
            adjectives.Add(8, "eighth");
            adjectives.Add(9, "ninth");
            adjectives.Add(10, "tenth");
            adjectives.Add(11, "eleventh");
            adjectives.Add(12, "twelfth");
            adjectives.Add(13, "thirteenth");
            adjectives.Add(14, "fourteenth");
            adjectives.Add(15, "fifteenth");
            adjectives.Add(16, "sixteenth");
            adjectives.Add(17, "seventeenth");
            adjectives.Add(18, "eighteenth");
            adjectives.Add(19, "nineteenth");
            adjectives.Add(20, "twentieth");
            adjectives.Add(30, "thirtieth");

            Func<int, string> getAdjective =
                i => { return capitalize ? adjectives[i].InitialCapital() : adjectives[i]; };

            var test = date.Day;
            // is it simple?
            if (test == 30 || test < 21)
            {
                return getAdjective(test);
            }

            var result = $"{(test < 30 ? "twenty" : "thirty")}-{adjectives[test%10]}";

            if (capitalize)
            {
                result = result.InitialCapital();
            }

            return result;
        }

        /// <summary>
        ///     Describes the day as a short adjective, eg '1st, 3rd, 10th, 29th'.
        /// </summary>
        /// <param name="date">The date.</param>
        /// <returns></returns>
        public static string DayAdjectiveShort(this DateTime date)
        {
            Func<int, string, string> getAdjective = (i, suffix) => i.ToString(CultureInfo.InvariantCulture) + suffix;

            var test = date.Day;

            if (test == 1 || test == 21 || test == 31)
            {
                return getAdjective(test, "st");
            }
            if (test == 2 || test == 22)
            {
                return getAdjective(test, "nd");
            }
            if (test == 3 || test == 23)
            {
                return getAdjective(test, "rd");
            }

            return getAdjective(test, "th");
        }

        /// <summary>
        ///     Calculates how many days of the week there are in a given month. For example.
        ///     '5 Sundays'.
        /// </summary>
        /// <param name="date">The date whose month you are targetting.</param>
        /// <param name="dayOfWeek">The day of week to check.</param>
        /// <returns></returns>
        public static int DayOfWeekCount(DateTime date, DayOfWeek dayOfWeek)
        {
            var dayOfMonth = new DateTime(date.Year, date.Month, 1);
            var count = 0;

            while (dayOfMonth.Month == date.Month)
            {
                if (dayOfMonth.DayOfWeek == dayOfWeek)
                {
                    count++;
                }
                dayOfMonth = dayOfMonth.AddDays(1);
            }

            return count;
        }

        #endregion
    }
}