﻿namespace ProofByDesign.Common.Utilities
{
    public static class MathExtensions
    {
        public static bool IsDebit(this decimal amount)
        {
            return amount >= 0;
        }

        public static bool IsCredit(this decimal amount)
        {
            return !IsDebit(amount);
        }
    }
}