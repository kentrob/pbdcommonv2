using System;

namespace ProofByDesign.Common.Utilities
{
    /// <summary>
    /// Represents a value from the config converted to the required type, with an error setting if
    /// value could not be converted.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class AppSetting<T> where T : IConvertible
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AppSetting&lt;T&gt;"/> class with the value 
        /// read from the config.
        /// </summary>
        /// <param name="value">The value.</param>
        public AppSetting(string value)
        {
            RawValue = value;

            try
            {
                Value = (T)Convert.ChangeType(value, typeof(T), System.Globalization.CultureInfo.InvariantCulture);
            }
            catch (Exception ex)
            {
                ConversionException = ex;
            }
        }

        /// <summary>
        /// Gets the converted value.
        /// </summary>
        public T Value { get; private set; }

        /// <summary>
        /// Gets a value indicating whether the value was converted to the correct type.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance has value; otherwise, <c>false</c>.
        /// </value>
        public bool HasValue
        {
            get
            {
                return typeof(T) == typeof(String) ? RawValue != null : !HasException;
            }
        }

        /// <summary>
        /// Gets the raw, underlying value for the convertible object.
        /// </summary>
        public string RawValue { get; private set; }

        /// <summary>
        /// Gets the conversion exception, if one occurred.
        /// </summary>
        public Exception ConversionException { get; private set; }

        /// <summary>
        /// Gets a value indicating whether this instance has a conversion exception.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance has exception; otherwise, <c>false</c>.
        /// </value>
        public bool HasException { get { return ConversionException != null; } }

    }
}
