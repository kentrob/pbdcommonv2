﻿using System;
using JetBrains.Annotations;

namespace ProofByDesign.Common.Utilities
{
    public class Check
    {
        ///// <summary>
        ///// This method is ten times slower than calling either NotNull(object) or NotEmpty(string).
        ///// It's a shame because it stops the developer having to specify the parameter name each time.
        ///// </summary>
        ///// <param name="exp"></param>
        //public static void NotEmpty(Expression<Func<object>> exp)
        //{
        //    var value = exp.Compile().Invoke();
        //    if (value == null || (value is string && value.ToString().IsNullOrWhitespace()))
        //    {
        //        var name = GetName(exp.Body);
        //        throw new ArgumentException(name);
        //    }
        //}

        //private static string GetName(Expression exp)
        //{
        //    var body = exp as MemberExpression;

        //    if (body == null)
        //    {
        //        var ubody = (UnaryExpression)exp;
        //        body = ubody.Operand as MemberExpression;
        //        if (body == null)
        //        {
        //            throw new InvalidOperationException(string.Format("Cannot find expression body in '{0}'.", exp));
        //        }
        //    }

        //    return body.Member.Name;
        //}

        [ContractAnnotation("value:null => halt")]
        public static void NotNull(object value, string parameterName)
        {
            if (value == null)
            {
                throw new ArgumentNullException(parameterName);
            }
        }

        [ContractAnnotation("value:null => halt")]
        public static void HasValue(object value, string parameterName, string errorMessage = null)
        {
            if (value == null || (value is string && value.ToString().IsNullOrWhitespace()))
            {
                throw new ArgumentException($"Argument {parameterName} is null or whitespace. {errorMessage}");
            }

            if (value is DateTime && DateHasDefaultValue((DateTime) value))
            {
                throw new ArgumentException($"Date argument {parameterName} has its default value {value} but should be set. {errorMessage}");
            }

            if (value is int)
                GreaterThanZero((int)value, parameterName, errorMessage);
            else if (value is decimal)
                GreaterThanZero((decimal)value, parameterName, errorMessage);
            else if (value is long)
                GreaterThanZero((long)value, parameterName, errorMessage);
            else if (value is double)
                GreaterThanZero((double)value, parameterName, errorMessage);
        }

        private static bool DateHasDefaultValue(DateTime value)
        {
            return value == DateTime.MinValue || value == DateUtility.MinDate;
        }

        public static void GreaterThanZero(long value, string parameterName, string errorMessage = "")
        {
            if (value <= 0)
            {
                throw new ArgumentOutOfRangeException($"Argument {parameterName} should be greater than zero. {errorMessage}");
            }
        }

        public static void GreaterThanZero(int value, string parameterName, string errorMessage = "")
        {
            if (value <= 0)
            {
                throw new ArgumentOutOfRangeException($"Argument {parameterName} should be greater than zero. {errorMessage}");
            }
        }
        public static void GreaterThanZero(decimal value, string parameterName, string errorMessage = "")
        {
            if (value <= 0)
            {
                throw new ArgumentOutOfRangeException($"Argument {parameterName} should be greater than zero. {errorMessage}");
            }
        }
        public static void GreaterThanZero(double value, string parameterName, string errorMessage = "")
        {
            if (value <= 0)
            {
                throw new ArgumentOutOfRangeException($"Argument {parameterName} should be greater than zero. {errorMessage}");
            }
        }
    }
}