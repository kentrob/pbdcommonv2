﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace ProofByDesign.Common.Utilities
{
    /// <summary>
    /// Provides encrytpion methods.
    /// </summary>
    public static class EncryptionHelper
    {

        # region public 

        /// <summary>
        /// Converts a hexadecimal string to a byte array.
        /// </summary>
        /// <param name="hexString"></param>
        /// <returns></returns>
        public static byte[] HexToByte(string hexString)
        {
            var returnBytes = new byte[hexString.Length / 2];
            for (var i = 0; i < returnBytes.Length; i++)
                returnBytes[i] = Convert.ToByte(hexString.Substring(i * 2, 2), 16);
            return returnBytes;
        }

        /// <summary>
        /// Returns a string representation of the hex values of a byte array.
        /// </summary>
        /// <param name="bytes"></param>
        /// <returns></returns>
        public static string ByteToHex(byte[] bytes)
        {
            var sb = new StringBuilder(bytes.Length * 2);
            foreach (var b in bytes)
            {
                sb.AppendFormat("{0:x2}", b);
            }

            return sb.ToString();
        }

        /// <summary>
        /// MD5 encrypts a string using UTF8Encoding and returns the hex value.
        /// </summary>
        /// <param name="data"></param>
        /// <returns>A hex string</returns>
        public static string MD5EncryptHex(string data)
        {            
            return MD5EncryptHex(data, new UTF8Encoding());
        }

        /// <summary>
        /// MD5 encrypts a string using the specified encoding and returns the hex value.
        /// </summary>
        /// <param name="data"></param>
        /// <param name="encoding">The encoding to use.</param>
        /// <returns>A hex string</returns>
        public static string MD5EncryptHex(string data, Encoding encoding)
        {
            return ByteToHex(MD5Encrypt(data, encoding));
        }

        /// <summary>
        /// MD5 encrypts a string using UTF8Encoding and returns the byte array.
        /// </summary>
        /// <param name="data"></param>
        /// <returns>A byte array.</returns>
        public static byte[] MD5Encrypt(string data)
        {
            return MD5Encrypt(data, new UTF8Encoding());
        }


        /// <summary>
        /// MD5 encrypts a string using the specified encoding and returns the byte array.
        /// </summary>
        /// <param name="data"></param>
        /// <param name="encoding">The encoding to use.</param>
        /// <returns>A byte array.</returns>
        public static byte[] MD5Encrypt(string data, Encoding encoding)
        {
            var md5Hasher = new MD5CryptoServiceProvider();
            return md5Hasher.ComputeHash(encoding.GetBytes(data));
        }

        #endregion
    }
}
