using System;
using System.Collections.Generic;
using System.Text;

namespace ProofByDesign.Common.Utilities
{
    public static class ListExtensions
    {
        /// <summary>
        /// Provides AddRange for an IList.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list">The list.</param>
        /// <param name="items">The items.</param>
        public static void AddRangeExt<T>(this IList<T> list, IEnumerable<T> items)
        {
            Check.HasValue(list, nameof(list));
            Check.HasValue(items, nameof(items));

            // Is it an underlying list? 
            var newList = list as List<T>;
            if (newList != null)
            {
                // List: Use native AddRange.
                newList.AddRange(items);
            }
            else
            {
                // Not a list so add manually.
                foreach (var item in items) list.Add(item);
            }
        }

        public static void RemoveRangeExt<T>(this IList<T> list, IEnumerable<T> items)
        {
            Check.HasValue(list, nameof(list));
            Check.HasValue(items, nameof(items));
            foreach (var item in items) list.Remove(item);
        }

        /// <summary>
        /// Reads the only.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list">The list.</param>
        /// <returns></returns>
        public static IList<T> AsReadOnlyExt<T>(this IList<T> list)
        {
            return IListAsList(list).AsReadOnly();
        }

        // ReSharper disable once InconsistentNaming
        private static List<T> IListAsList<T>(IList<T> list)
        {
            var newList = list as List<T>;

            if (newList == null)
            {
                newList = new List<T>();
                newList.AddRange(list);
            }

            return newList;
        }

        public static void ForEachInSequence<T>(this IEnumerable<T> list, Action<T> action)
        {
            if (list == null)
                return;

            Check.HasValue(action, nameof(action));

            foreach (var item in list)
            {
                action(item);
            }
        }

        public static string ToFormattedString(this List<KeyValuePair<string, string>> list, string separator = ": ", string lineEnd = "\n")
        {
            var sb = new StringBuilder();
            foreach (var valuePair in list)
            {
                sb.AppendFormat("{0}{1}", valuePair.Key, separator);
                sb.AppendFormat("{0}{1}", valuePair.Value, lineEnd);
            }
            return sb.ToString();
        }
    }
}
