﻿using System;

namespace ProofByDesign.Common
{
    public interface IIdName
    {
        Int64 Id { get; }
        string Name { get; }
        IdName AsIdName();
    }
}